/*
clank-client.js

clank controller client side

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2020

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the open systems assembly protocol (OSAP) project.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

'use strict'

import Grid from './utes/grid.js' // main drawing API 
import { Button, EZButton, Slider, TextBlock, TextInput } from './utes/basics.js'

// import { SaveFile } from '../osapjs/client/utes/saveFile.js'
// import { GetFile } from '../osapjs/client/utes/getFile.js'

// ... yar ok,
// import ImgToPath2D from './gerberToImage.js'
// import MachineBed from '../osapjs/client/components/machineBed.js'

/*
import Pad from '../osapjs/client/components/pad.js'
import AXLClankVM from './axlClankVM.js'

// test / etc... 
import { flattenSVG } from "./svglib/flattenSVG.js"
import testPath from './save/pathTest.js'
import testSVGs from './svglib/svgLazyImport.js'
*/

console.log("it's all pipes, baby")

// -------------------------------------------------------- Grid 

let grid = new Grid()

// -------------------------------------------------------- Build a Box

let settings = {
  xPlace: 10,
  yPlace: 10,
  width: 500,
  height: 500,
}

let dom = $('.plane').get(0)
let elem = $('<div>')
  .css('position', 'absolute')
  .css('width', `${settings.width}px`)
  .css('height', `${settings.height}px`)
  .css('background-color', '#fff')
  .css('left', `${settings.xPlace}px`)
  .css('top', `${settings.yPlace}px`)
  .css('border', `1px solid rgb(225, 225, 225)`)
  .get(0)
$(dom).append(elem)

let colX = settings.xPlace
let colY = settings.yPlace + settings.height + 10

// build a reporting block, 
let messageBox = new TextBlock({
  xPlace: colX,
  yPlace: colY,
  width: settings.width,
  height: 30,
  defaultText: `...`
}, true)

// -------------------------------------------------------- Drag, and Drop 

$(elem).on('dragover', (evt) => {
  // console.log('dragover', evt)
  evt.preventDefault()
})

$(elem).on('drop', (evt) => {
  // walk over jquery's bottle 
  evt = evt.originalEvent
  evt.preventDefault()
  // rm our old layers... eventually we could check-guard against rm'ing stateful things here, 
  // for (let layer of layers) {
  //   layer.btn.remove()
  //   layer.remove()
  // }
  // get for-items... 
  if (evt.dataTransfer.items) {
    [...evt.dataTransfer.items].forEach((item, i) => {
      if (item.kind === 'file') {
        // ~ to use the File API 
        let file = item.getAsFile()
        console.log(`… file[${i}].name = ${file.name}`)
        // filter for names... 
        let layerName = ''
        if (file.name.includes('trace') || file.name.includes('copper_top')) {
          layerName = 'topTraces'
        } else if (file.name.includes('interior') || file.name.includes('profile')) {
          layerName = 'outline'
        } else {
          console.error(`no known layer type for ${file.name}, bailing...`)
        }
        // ok, now switch-import on types, 
        if (file.name.includes('.gbr')) {
          // we want to get it as... 
          let reader = new FileReader()
          reader.addEventListener('load', async () => {
            try {
              let gerber = reader.result 
              console.warn("loaded a gerber as text:", gerber)
              let svg = await gerberToSVG(gerber)
              console.warn("converted the to SVG:", svg)  
              // we can get the real width / height of the file here:
              let realWidth = parseFloat($(svg).attr('width'))
              let realHeight = parseFloat($(svg).attr('height'))
              console.warn(`SVG has width / height (mm): ${realWidth}, ${realHeight}`)
              let image = await svgToImage(svg)
              console.warn("converted svg to image:", image)
              let canvas = document.createElement('canvas')
              // we can pick a pixels-per-mm here, 
              let ppmm = 20
              let width = realWidth * ppmm
              let height = realHeight * ppmm
              canvas.width = width
              canvas.height = height
              let context = canvas.getContext('2d');   // draw image in canvas starting left-0 , top - 0     
              // we can scale the context as we draw it into the canvas, 
              context.drawImage(image, 0, 0, width * 0.5, height * 0.5);
              console.log('into context', context)
              $(elem).append(canvas)
          
            } catch (err) { 
              console.error('error during gerber reader onLoad handler', err)
            }
          })
          reader.addEventListener('error', (err) => {
            console.error(`gerber file-reader error;`, err)
          })
          reader.readAsText(file)
          // end if-gerber 
        } else if (file.name.includes('.png')) {
          // use a fileReaderto get the data, 
          // this could be a ute... 
          /*
          let reader = new FileReader()
          reader.addEventListener('load', () => {
            // console.log(reader.result)
            // we want to collect an ImageData from this thing, 
            let image = new Image()
            image.onload = () => {
              console.log(image)
              let canvas = document.createElement('canvas')
              canvas.width = image.width
              canvas.height = image.height
              let context = canvas.getContext('2d')
              context.drawImage(image, 0, 0, image.width, image.height)
              let imageData = context.getImageData(0, 0, image.width, image.height)
              console.log(imageData)
              this.addLayer({
                name: layerName,
                imageData: imageData,
                dpi: 1000,
              })
            } // end image onload 
            image.onerror = (err) => {
              console.error(err)
            }
            image.src = reader.result
          })
          reader.addEventListener('error', (err) => {
            console.error(err)
          })
          reader.readAsDataURL(file)
          */
        } else { // end .png case 
          console.error(`unknown file type encountered ${file.name}`)
        }
      }
    })
  } else {
    console.error(`jake hasn't handled this case...`)
    // see https://developer.mozilla.org/en-US/docs/Web/API/HTML_Drag_and_Drop_API/File_drag_and_drop 
  }
})

let gerberToSVG = (input, options) => {
  return new Promise((resolve, reject) => {
    if (!options) options = { encoding: 'utf8' }
    let converter = gerberToSvg(input, options, (err, svg) => {
      if (err) {
        reject(err)
      } else {
        // the SVG has .attr for viewbox, anchors, units, etc, 
        // see https://github.com/tracespace/tracespace/blob/main/packages/gerber-to-svg/API.md#output 
        converter.svg = svg 
        resolve(svg)
      }
    })
  })
}

let svgToImage = async (svg) => {
  try {
    let blob = new Blob([svg], { type: 'image/svg+xml;charset=utf-8' })
    console.log('have a blob...', blob)
    // url of the blob, 
    let blobURL = (window.URL || window.webkitURL || window).createObjectURL(blob);
    console.log('have blob url...', blobURL)
    // get an image object, 
    let image = await svgImageLoader(blobURL)
    console.log('awaited image...', image)
    return image 
  } catch (err) {
    throw err 
  }
}

let svgImageLoader = (source) => {
  return new Promise((resolve, reject) => {
    let image = new Image()
    image.onload = () => {
      resolve(image)
    }
    image.onerror = (err) => {
      reject(`failed to load image with source ${source}`)
    }
    image.src = source
  })
}

  


console.warn(`----------------- DONE`)