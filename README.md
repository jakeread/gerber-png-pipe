# Gerber to PNG applet (G2P)

## Description

"never export PNGs from KiCAD or EAGLE again !" 
-Jake Read 

An applet used to translate Gerber layers exported from an ECAD tool into a Portable Networks Graphic (PNG) file.

### Example usage 
![gerbs](docs/2022-10-05_gerb-to-png.mp4)

Accesible at https://jakeread.pages.cba.mit.edu/gerber-png-pipe/site/

### To run locally

Clone the repo and use a static webserver from `site/` - i.e. [`http-server`](https://www.npmjs.com/package/http-server)

### Export files

To export a PNG, right click the displayed graphic and use the "Save Image As..." feature in the context menu.
